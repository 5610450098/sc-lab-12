package phonebook;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class PhoneBookMain {
	public static void main(String[] args){
		String filename = "phonebook.txt";
		ArrayList<PhoneBook> phonebook = new ArrayList<PhoneBook>();
		
		try{
			FileReader fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);
			
			System.out.println("------ Java Phone Book ------");
			System.out.println("Name\tPhone");
			System.out.println("=====\t==============");
			
			String line;
			for(line = buffer.readLine() ; line != null ; line = buffer.readLine()){
				String[] data = line.split(",");
				String name = data[0].trim();
				String num = data[1].trim();
				PhoneBook p = new PhoneBook(name,num);
				phonebook.add(p);
			}
			for(PhoneBook p : phonebook){
				System.out.println(p);
			}
		}
		catch(FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}
		catch(IOException e){
			System.err.println("Error reading from file");
		}
	}
}
