package phonebook;

public class PhoneBook {
	private String name;
	private String phoneNum;
	
	public PhoneBook(String name,String num){
		this.name = name;
		phoneNum = num;
	}
	
	public String toString(){
		return name+"\t"+phoneNum;
	}
}	
