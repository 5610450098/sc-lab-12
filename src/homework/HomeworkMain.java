package homework;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class HomeworkMain {
	public static void main(String[] args){
		String filename = "homework.txt";
		ArrayList<Homework> homework = new ArrayList<Homework>();
		
		try{
			FileReader fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);
			FileWriter fileWriter = new FileWriter("average.txt");
			PrintWriter out = new PrintWriter(new BufferedWriter(fileWriter));
			
			out.println("------ Homework Scores ------");
			out.println("Name\tAverage");
			out.println("=====\t==============");
			
			String line;
			for(line = buffer.readLine() ; line != null ; line = buffer.readLine()){
				ArrayList<String> lstScore = new ArrayList<String>();
				for(String s: line.split(",")){
					lstScore.add(s.trim());
				}
				Homework h = new Homework(lstScore.get(0));
				for(int i=0;i<lstScore.size();i++){
					if(i!=0){
						h.addScore(Double.parseDouble(lstScore.get(i)));
					}
				}
				homework.add(h);
			}
			for(Homework h : homework){
				out.println(h);
			}
			out.close();
			
			FileReader filePrint = new FileReader("average.txt");
			BufferedReader bufferPrint = new BufferedReader(filePrint);
			String line2;
			for(line2 = bufferPrint.readLine() ; line2 != null ; line2 = bufferPrint.readLine()){
				System.out.println(line2);
			}
			
		}
		catch(FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}
		catch(IOException e){
			System.err.println("Error reading from file");
		}
	}
}
