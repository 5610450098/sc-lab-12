package homework;

import java.util.ArrayList;

public class Homework {
	private String name;
	private ArrayList<Double> score;
	
	public Homework(String name){
		this.name = name;
		score = new ArrayList<Double>();
	}
	
	public double average(){
		double i = 0;
		for(double d:score){
			i+=d;
		}
		return i/score.size();
	}
	public void addScore(double s){
		score.add(s);
	}
	
	public String toString(){
		return name+"\t"+average();
	}
}
