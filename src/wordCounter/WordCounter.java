package wordCounter;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class WordCounter {
	private String message;
	private Map<String,Integer> wordCounter;
	public WordCounter(String message){
		this.message = message;
		wordCounter = new HashMap<String,Integer>();
	}
	public void count(){
		try{
			FileReader fileReader = new FileReader(message);
			BufferedReader buffer = new BufferedReader(fileReader);
			String line;
			for(line = buffer.readLine() ; line != null ; line = buffer.readLine()){
				for(String s:line.split(" ")){
					if(wordCounter.containsKey(s)==true){
						wordCounter.put(s,wordCounter.get(s)+1);
					}
					else{
						wordCounter.put(s,1);
					}
				}
			}
		}
		catch(FileNotFoundException e){
			System.err.println("Cannot read file "+message);
		}
		catch(IOException e){
			System.err.println("Error reading from file");
		}
	}
	
	public String getCountData(){
		String s = "";
		for(Map.Entry<String,Integer> word : wordCounter.entrySet()){
			s+=word+"\n";
		}
		return s;
	}
	
}
